//
//  TransactionListViewController.h
//  ExpenseIncomeTracker
//
//  Created by Jordan Hipwell on 2/7/15.
//  Copyright (c) 2015 Jordan Hipwell. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, TransactionType) {
    Expenses,
    Income
};

@interface TransactionListViewController : UIViewController

- (instancetype)initWithType:(TransactionType)type;

@end
