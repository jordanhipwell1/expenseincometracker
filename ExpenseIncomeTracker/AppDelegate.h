//
//  AppDelegate.h
//  ExpenseIncomeTracker
//
//  Created by Jordan Hipwell on 2/7/15.
//  Copyright (c) 2015 Jordan Hipwell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) NSMutableArray *expensesArray;
@property (strong, nonatomic) NSMutableArray *incomeArray;

extern NSString * const TransactionTypeIdentifier;
extern NSString * const TitleIdentifier;
extern NSString * const AmountIdentifier;
extern NSString * const DateIdentifier;
extern NSString * const ImagePathIdentifier;

@end

