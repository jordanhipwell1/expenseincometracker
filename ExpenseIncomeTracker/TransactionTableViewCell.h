//
//  TransactionTableViewCell.h
//  ExpenseIncomeTracker
//
//  Created by Jordan Hipwell on 2/7/15.
//  Copyright (c) 2015 Jordan Hipwell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TransactionTableViewCell : UITableViewCell

@property (nonatomic, strong) UILabel *transactionTitleLabel;
@property (nonatomic, strong) UILabel *transactionPriceLabel;

@end
