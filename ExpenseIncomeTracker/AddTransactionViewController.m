//
//  AddTransactionViewController.m
//  ExpenseIncomeTracker
//
//  Created by Jordan Hipwell on 2/7/15.
//  Copyright (c) 2015 Jordan Hipwell. All rights reserved.
//

#import "AddTransactionViewController.h"
#import "AppDelegate.h"
#import <MessageUI/MessageUI.h>

@interface AddTransactionViewController () <UITextFieldDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, MFMailComposeViewControllerDelegate> {
    TransactionType transactionType;
}

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UITextField *transactionTitle;
@property (nonatomic, strong) UITextField *transactionAmount;
@property (nonatomic, strong) UIDatePicker *datePicker;
@property (nonatomic, strong) UIButton *photoButton;
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) NSLayoutConstraint *imageViewHeightConstraint;

@property (nonatomic, strong) NSString *imagePath;

@end

@implementation AddTransactionViewController

- (instancetype)initWithTransactionType:(TransactionType)type {
    if (self = [super init]) {
        transactionType = type;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.edgesForExtendedLayout = UIRectEdgeNone; //move content under nav bar
    self.navigationController.view.backgroundColor = [UIColor whiteColor];
    
    //bar button items
    if ([self isModal]) { //if not being pushed
        UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                                                                      target:self
                                                                                      action:@selector(didTapCancel:)];
        self.navigationItem.leftBarButtonItem = cancelButton;
        
        UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave
                                                                                    target:self
                                                                                    action:@selector(didTapSave:)];
        self.navigationItem.rightBarButtonItem = saveButton;
        
        if (transactionType == Expenses) {
            self.title = @"Add Expense";
        } else {
            self.title = @"Add Income";
        }
    } else {
        if (transactionType == Expenses) {
            self.title = @"Expense Transaction";
        } else {
            self.title = @"Income Transaction";
        }
        
        if ([MFMailComposeViewController canSendMail]) {
            UIBarButtonItem *shareButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction
                                                                                         target:self
                                                                                         action:@selector(didTapShare:)];
            self.navigationItem.rightBarButtonItem = shareButton;
        }
    }
    
    //scroll view
    self.scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    self.scrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:self.scrollView];
    
    //transaction title
    UITextField *transactionTitle = [[UITextField alloc] init];
    transactionTitle.placeholder = @"Transaction Title";
    transactionTitle.textAlignment = NSTextAlignmentCenter;
    transactionTitle.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    transactionTitle.text = self.transactionInfo[TitleIdentifier];
    self.transactionTitle = transactionTitle;
    [self.scrollView addSubview:transactionTitle];
    
    transactionTitle.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:transactionTitle
                                                          attribute:NSLayoutAttributeLeading
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeLeading
                                                         multiplier:1
                                                           constant:15]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:transactionTitle
                                                          attribute:NSLayoutAttributeTrailing
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeTrailing
                                                         multiplier:1
                                                           constant:-15]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:transactionTitle
                                                          attribute:NSLayoutAttributeTop
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.scrollView
                                                          attribute:NSLayoutAttributeTop
                                                         multiplier:1
                                                           constant:15]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:transactionTitle
                                                          attribute:NSLayoutAttributeHeight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:nil
                                                          attribute:NSLayoutAttributeHeight
                                                         multiplier:1
                                                           constant:50]];
    
    //dollar sign label
    UILabel *dollarSignLabel = [[UILabel alloc] init];
    dollarSignLabel.text = @"$";
    dollarSignLabel.font = [UIFont systemFontOfSize:20];
    dollarSignLabel.userInteractionEnabled = NO;
    [self.scrollView addSubview:dollarSignLabel];
    
    dollarSignLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:dollarSignLabel
                                                          attribute:NSLayoutAttributeLeading
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeLeading
                                                         multiplier:1
                                                           constant:15]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:dollarSignLabel
                                                          attribute:NSLayoutAttributeTop
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:transactionTitle
                                                          attribute:NSLayoutAttributeBottom
                                                         multiplier:1
                                                           constant:20]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:dollarSignLabel
                                                          attribute:NSLayoutAttributeHeight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:nil
                                                          attribute:NSLayoutAttributeHeight
                                                         multiplier:1
                                                           constant:50]];
    
    //transaction price
    UITextField *transactionAmount = [[UITextField alloc] init];
    transactionAmount.keyboardType = UIKeyboardTypeDecimalPad;
    transactionAmount.borderStyle = UITextBorderStyleRoundedRect;
    transactionAmount.delegate = self;
    transactionAmount.text = self.transactionInfo[AmountIdentifier];
    [self.scrollView addSubview:transactionAmount];
    self.transactionAmount = transactionAmount;
    
    transactionAmount.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:transactionAmount
                                                          attribute:NSLayoutAttributeLeading
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:dollarSignLabel
                                                          attribute:NSLayoutAttributeLeading
                                                         multiplier:1
                                                           constant:20]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:transactionAmount
                                                          attribute:NSLayoutAttributeTrailing
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeTrailing
                                                         multiplier:1
                                                           constant:-15]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:transactionAmount
                                                          attribute:NSLayoutAttributeTop
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:transactionTitle
                                                          attribute:NSLayoutAttributeBottom
                                                         multiplier:1
                                                           constant:20]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:transactionAmount
                                                          attribute:NSLayoutAttributeHeight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:nil
                                                          attribute:NSLayoutAttributeHeight
                                                         multiplier:1
                                                           constant:50]];
    
    UIDatePicker *datePicker = [[UIDatePicker alloc] init];
    datePicker.datePickerMode = UIDatePickerModeDate;
    datePicker.date = self.transactionInfo[DateIdentifier] ? self.transactionInfo[DateIdentifier] : [NSDate date];
    [datePicker addTarget:self action:@selector(datePickerValueDidChange:) forControlEvents:UIControlEventValueChanged];
    [self.scrollView addSubview:datePicker];
    self.datePicker = datePicker;
    
    datePicker.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:datePicker
                                                          attribute:NSLayoutAttributeLeading
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeLeading
                                                         multiplier:1
                                                           constant:0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:datePicker
                                                          attribute:NSLayoutAttributeTrailing
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeTrailing
                                                         multiplier:1
                                                           constant:0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:datePicker
                                                          attribute:NSLayoutAttributeTop
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:transactionAmount
                                                          attribute:NSLayoutAttributeBottom
                                                         multiplier:1
                                                           constant:20]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:datePicker
                                                          attribute:NSLayoutAttributeHeight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:nil
                                                          attribute:NSLayoutAttributeHeight
                                                         multiplier:1
                                                           constant:162]];
    
    //take photo button
    self.photoButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [self.photoButton addTarget:self action:@selector(didTapPhotoButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollView addSubview:self.photoButton];
    
    self.photoButton.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.photoButton
                                                          attribute:NSLayoutAttributeTop
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:datePicker
                                                          attribute:NSLayoutAttributeBottom
                                                         multiplier:1
                                                           constant:20]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.photoButton
                                                          attribute:NSLayoutAttributeCenterX
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:transactionTitle
                                                          attribute:NSLayoutAttributeCenterX
                                                         multiplier:1
                                                           constant:0]];
    
    //image view
    self.imageView = [[UIImageView alloc] init];
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
    self.imageView.image = [self imageAtPath:self.transactionInfo[ImagePathIdentifier]];
    [self.scrollView addSubview:self.imageView];
    
    self.imageView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.imageView
                                                          attribute:NSLayoutAttributeLeading
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeLeading
                                                         multiplier:1
                                                           constant:0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.imageView
                                                          attribute:NSLayoutAttributeTrailing
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeTrailing
                                                         multiplier:1
                                                           constant:0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.imageView
                                                          attribute:NSLayoutAttributeTop
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.photoButton
                                                          attribute:NSLayoutAttributeBottom
                                                         multiplier:1
                                                           constant:20]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.imageView
                                                          attribute:NSLayoutAttributeBottom
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.scrollView
                                                          attribute:NSLayoutAttributeBottom
                                                         multiplier:1
                                                           constant:0]];
    self.imageViewHeightConstraint = [NSLayoutConstraint constraintWithItem:self.imageView
                                                                  attribute:NSLayoutAttributeHeight
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:nil
                                                                  attribute:NSLayoutAttributeHeight
                                                                 multiplier:1
                                                                   constant:250];
    [self.view addConstraint:self.imageViewHeightConstraint];
    
    if (self.imageView.image == nil) {
        [self.photoButton setTitle:@"Add Photo" forState:UIControlStateNormal];
        self.imageViewHeightConstraint.constant = 0;
    } else {
        [self.photoButton setTitle:@"Remove Photo" forState:UIControlStateNormal];
    }
    
    //disable interaction if viewing existing transaction
    if (self.transactionInfo != nil) {
        self.transactionTitle.enabled = NO;
        self.transactionAmount.enabled = NO;
        self.datePicker.enabled = NO;
        self.datePicker.userInteractionEnabled = NO;
        self.photoButton.enabled = NO;
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if ([textField isEqual:self.transactionAmount]) {
        //validation - only one decimal with two digits after
        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
        NSString *decimalSymbol = [formatter decimalSeparator];
        
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        NSString *expression = [NSString stringWithFormat:@"^([0-9]+)?(\\%@([0-9]{1,2})?)?$", decimalSymbol];
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression
                                                                               options:NSRegularExpressionCaseInsensitive
                                                                                 error:nil];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString
                                                            options:0
                                                              range:NSMakeRange(0, [newString length])];
        if (numberOfMatches == 0) {
            return NO;
        }
    }
    
    return YES;
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    
    NSDate *currentDate = [NSDate date];
    NSDateFormatter *dateFormater = [[NSDateFormatter alloc] init];
    dateFormater.dateFormat = @"yyyy-MM-DD HH:mm:ss";
    NSString *dateString = [dateFormater stringFromDate:currentDate];
    
    self.imagePath = [self saveImage:image withTimeStamp:dateString];
    self.imageView.image = [self imageAtPath:self.imagePath];
    
    self.imageViewHeightConstraint.constant = 250;
    [self.photoButton setTitle:@"Remove Photo" forState:UIControlStateNormal];
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - Methods

- (void)datePickerValueDidChange:(UIDatePicker *)datePicker {
    NSLog(@"%@", datePicker.date);
}

- (void)didTapShare:(UIBarButtonItem *)sender {
    MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
    controller.mailComposeDelegate = self;
    [controller setSubject:@"Transaction"];
    [controller setMessageBody:[NSString stringWithFormat:@"%@ - %@", self.transactionAmount, self.transactionTitle] isHTML:NO];
    [self presentViewController:controller animated:YES completion:NULL];
}

- (void)didTapCancel:(UIBarButtonItem *)sender {
    [self.view endEditing:NO];
    [self.presentingViewController dismissViewControllerAnimated:YES completion:NULL];
}

- (void)didTapSave:(UIBarButtonItem *)sender {
    [self.view endEditing:NO];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DidAddNewTransaction"
                                                        object:@{TransactionTypeIdentifier: [NSNumber numberWithLong:transactionType],
                                                                 TitleIdentifier: self.transactionTitle.text,
                                                                 AmountIdentifier: self.transactionAmount.text,
                                                                 DateIdentifier: self.datePicker.date,
                                                                 ImagePathIdentifier: ObjectOrNull(self.imagePath)}];
    
    [self.presentingViewController dismissViewControllerAnimated:YES completion:NULL];
}

- (void)didTapPhotoButton:(UIButton *)sender {
    if (self.imageView.image != nil) {
        self.imageView.image = nil;
        [self.photoButton setTitle:@"Add Photo" forState:UIControlStateNormal];
        self.imageViewHeightConstraint.constant = 0;
    } else {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                       message:nil
                                                                preferredStyle:UIAlertControllerStyleActionSheet];
        
        UIAlertAction *camera = [UIAlertAction actionWithTitle:@"Camera"
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction *action) {
                                                           [self showImagePickerWithType:UIImagePickerControllerSourceTypeCamera];
                                                       }];
        [alert addAction:camera];
        
        UIAlertAction *photoLibrary = [UIAlertAction actionWithTitle:@"Photo Library"
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction *action) {
                                                                 [self showImagePickerWithType:UIImagePickerControllerSourceTypePhotoLibrary];
                                                             }];
        [alert addAction:photoLibrary];
        
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                         style:UIAlertActionStyleCancel
                                                       handler:nil];
        [alert addAction:cancel];
        
        [self presentViewController:alert animated:YES completion:NULL];
        
        //auto dismiss upon app close
        [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationWillResignActiveNotification
                                                          object:nil
                                                           queue:[NSOperationQueue mainQueue]
                                                      usingBlock:^(NSNotification *notification) {
                                                          [alert dismissViewControllerAnimated:NO completion:NULL];
                                                      }];
    }
}

#pragma mark - Helper Methods

- (void)showImagePickerWithType:(UIImagePickerControllerSourceType)type {
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.sourceType = type;
    imagePicker.delegate = self;
    [self presentViewController:imagePicker animated:YES completion:NULL];
}

- (UIImage *)imageAtPath:(NSString*)pathString {
    if (pathString && ![pathString isEqual:[NSNull null]]) {
        return [UIImage imageWithContentsOfFile:pathString];
    }
    return nil;
}

- (NSString *)saveImage:(UIImage *)image withTimeStamp:(NSString *)timeStamp {
    NSData *imageData = UIImagePNGRepresentation(image);
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *imagePath =[documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", timeStamp]];
    
    if (![imageData writeToFile:imagePath atomically:NO]) {
        NSLog(@"Failed to save image to path: %@", imagePath);
    } else {
        return imagePath;
    }
    
    return nil;
}

- (BOOL)isModal {
    if([self presentingViewController])
        return YES;
    if([[self presentingViewController] presentedViewController] == self)
        return YES;
    if([[[self navigationController] presentingViewController] presentedViewController] == [self navigationController])
        return YES;
    if([[[self tabBarController] presentingViewController] isKindOfClass:[UITabBarController class]])
        return YES;
    
    return NO;
}

static id ObjectOrNull(id object) {
    return object ?: [NSNull null];
}


@end
