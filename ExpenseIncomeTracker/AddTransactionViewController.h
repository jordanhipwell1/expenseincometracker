//
//  AddTransactionViewController.h
//  ExpenseIncomeTracker
//
//  Created by Jordan Hipwell on 2/7/15.
//  Copyright (c) 2015 Jordan Hipwell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransactionListViewController.h"

@interface AddTransactionViewController : UIViewController

@property (nonatomic, strong) NSDictionary *transactionInfo;

- (instancetype)initWithTransactionType:(TransactionType)type;

@end
