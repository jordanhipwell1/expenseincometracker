//
//  TransactionListViewController.m
//  ExpenseIncomeTracker
//
//  Created by Jordan Hipwell on 2/7/15.
//  Copyright (c) 2015 Jordan Hipwell. All rights reserved.
//

#import "TransactionListViewController.h"
#import "AddTransactionViewController.h"
#import "TransactionTableViewCell.h"
#import "AppDelegate.h"

@interface TransactionListViewController () <UITableViewDataSource, UITableViewDelegate> {
    TransactionType transactionType;
}

@property (nonatomic, weak) NSMutableArray *transactionsArray;
@property (nonatomic, strong) UITableView *tableView;

@end

@implementation TransactionListViewController

- (instancetype)initWithType:(TransactionType)type {
    if (self = [super init]) {
        transactionType = type;
        
        if (type == Expenses) {
            self.title = @"Expenses";
            self.tabBarItem.image = [UIImage imageNamed:@"credit-card"];
        } else {
            self.title = @"Income";
            self.tabBarItem.image = [UIImage imageNamed:@"piggy"];
        }
    }
    
    return self;
}

- (void)viewDidLoad {
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                                                               target:self
                                                                               action:@selector(didTapAdd:)];
    self.navigationItem.rightBarButtonItem = addButton;
    
    UITableView *tableView = [[UITableView alloc] initWithFrame:self.view.bounds];
    tableView.dataSource = self;
    tableView.delegate = self;
    tableView.rowHeight = UITableViewAutomaticDimension;
    tableView.estimatedRowHeight = 44.0;
    tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [tableView registerClass:[TransactionTableViewCell class] forCellReuseIdentifier:@"cell"];
    [self.view addSubview:tableView];
    self.tableView = tableView;
    
    //get existing data from app delegate
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (transactionType == Expenses) {
        self.transactionsArray = appDelegate.expensesArray; //point to same mutable array
    } else {
        self.transactionsArray = appDelegate.incomeArray;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didAddNewEntry:)
                                                 name:@"DidAddNewTransaction"
                                               object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    //enable interactive deselection via swipe back gesture
    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
    
    if (transactionType == Expenses) {
        self.tabBarController.tabBar.tintColor = [UIColor redColor];
    } else {
        self.tabBarController.tabBar.tintColor = [UIColor colorWithRed:10/255.0 green:200/255.0 blue:50/255.0 alpha:1.0];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.transactionsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TransactionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    cell.transactionTitleLabel.text = self.transactionsArray[indexPath.row][TitleIdentifier];
    cell.transactionTitleLabel.textColor = [UIColor darkTextColor];

    cell.transactionPriceLabel.text = [NSString stringWithFormat:@"$%@", self.transactionsArray[indexPath.row][AmountIdentifier]];
    cell.transactionPriceLabel.textColor = self.tabBarController.tabBar.tintColor;
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    AddTransactionViewController *atvc = [[AddTransactionViewController alloc] initWithTransactionType:transactionType];
    
    atvc.transactionInfo = self.transactionsArray[indexPath.row];
    
    [self.navigationController pushViewController:atvc animated:YES];
}

#pragma mark - Methods

- (void)didTapAdd:(UIBarButtonItem *)sender {
    AddTransactionViewController *addTransactionVC = [[AddTransactionViewController alloc] initWithTransactionType:transactionType];
    UINavigationController *addTransactionNavController = [[UINavigationController alloc] initWithRootViewController:addTransactionVC];
    [self presentViewController:addTransactionNavController animated:YES completion:NULL];
}

- (void)didAddNewEntry:(NSNotification *)notification {
    NSDictionary *notificationObject = notification.object;
    if ([notificationObject[TransactionTypeIdentifier] isEqualToNumber:[NSNumber numberWithLong:transactionType]]) {
        NSDictionary *newEntryData = @{TitleIdentifier: notificationObject[TitleIdentifier],
                                       AmountIdentifier: notificationObject[AmountIdentifier],
                                       DateIdentifier: notificationObject[DateIdentifier],
                                       ImagePathIdentifier: notificationObject[ImagePathIdentifier]};
        [self.transactionsArray addObject:newEntryData]; //add new entry to array
    }
}

@end
