//
//  ViewController.m
//  ExpenseIncomeTracker
//
//  Created by Jordan Hipwell on 2/7/15.
//  Copyright (c) 2015 Jordan Hipwell. All rights reserved.
//

#import "ViewController.h"
#import "TransactionListViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITabBarController *tabBarController = [[UITabBarController alloc] init];
    
    TransactionListViewController *expensesVC = [[TransactionListViewController alloc] initWithType:Expenses];
    TransactionListViewController *incomeVC = [[TransactionListViewController alloc] initWithType:Income];
    
    UINavigationController *expensesNavController = [[UINavigationController alloc] initWithRootViewController:expensesVC];
    UINavigationController *incomeNavController = [[UINavigationController alloc] initWithRootViewController:incomeVC];
    
    tabBarController.viewControllers = @[expensesNavController, incomeNavController];
    
    [self.view addSubview:tabBarController.view];
    [self addChildViewController:tabBarController];
}

@end
