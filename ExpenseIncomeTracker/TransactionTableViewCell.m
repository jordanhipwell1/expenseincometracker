//
//  TransactionTableViewCell.m
//  ExpenseIncomeTracker
//
//  Created by Jordan Hipwell on 2/7/15.
//  Copyright (c) 2015 Jordan Hipwell. All rights reserved.
//

#import "TransactionTableViewCell.h"

@interface TransactionTableViewCell ()

@property (nonatomic, assign) BOOL didUpdateConstraints;

@end


@implementation TransactionTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {

    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.translatesAutoresizingMaskIntoConstraints = NO;
        
        self.transactionPriceLabel = [[UILabel alloc] init];
        self.transactionPriceLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:self.transactionPriceLabel];
        
        self.transactionTitleLabel = [[UILabel alloc] init];
        self.transactionTitleLabel.numberOfLines = 2;
        self.transactionTitleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:self.transactionTitleLabel];
    }
    
    self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    [self setNeedsUpdateConstraints];
    
    return self;
    
}

- (void)updateConstraints {
    if (!self.didUpdateConstraints) {
        self.didUpdateConstraints = YES;
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.transactionPriceLabel
                                                                     attribute:NSLayoutAttributeTrailing
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.contentView
                                                                     attribute:NSLayoutAttributeTrailing
                                                                    multiplier:1
                                                                      constant:-15]];
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.transactionPriceLabel
                                                                     attribute:NSLayoutAttributeTop
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.contentView
                                                                     attribute:NSLayoutAttributeTop
                                                                    multiplier:1
                                                                      constant:15]];
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.transactionPriceLabel
                                                                     attribute:NSLayoutAttributeBottom
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.contentView
                                                                     attribute:NSLayoutAttributeBottom
                                                                    multiplier:1
                                                                      constant:-15]];
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.transactionTitleLabel
                                                                     attribute:NSLayoutAttributeLeading
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.contentView
                                                                     attribute:NSLayoutAttributeLeading
                                                                    multiplier:1
                                                                      constant:15]];
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.transactionTitleLabel
                                                                     attribute:NSLayoutAttributeTop
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.contentView
                                                                     attribute:NSLayoutAttributeTop
                                                                    multiplier:1
                                                                      constant:15]];
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.transactionTitleLabel
                                                                     attribute:NSLayoutAttributeBottom
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.contentView
                                                                     attribute:NSLayoutAttributeBottom
                                                                    multiplier:1
                                                                      constant:-15]];
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.transactionTitleLabel
                                                                     attribute:NSLayoutAttributeTrailing
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.transactionPriceLabel
                                                                     attribute:NSLayoutAttributeLeft
                                                                    multiplier:1
                                                                      constant:-10]];
    }
    
    [super updateConstraints];
}

@end
